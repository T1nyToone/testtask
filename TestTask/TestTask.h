#include <string>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;


class chaptersModel
{

public:
	string buffer;	//buffer for readed file from file
	vector <string> chaptersHeader;	//stores all headers of chapters

	/*The sequance of 3 vectors: */
	vector <int> chapterNumber;	// 1 - chapter number
	vector <string> chapterChoose; // 2 - choose; 
	vector <string> chapterLink; // 3 - link of choose

	void parseElement(int colomn) // parse a current element (colomn=1 - for vector chapterChoose, colomn=2 - for chapterlink)
	{
		int index=0;
		index=buffer.find(';');
		if(colomn==1)
			chapterChoose.push_back(buffer.substr(0,index));
		else
			chapterLink.push_back(buffer.substr(0,index));

		buffer=buffer.erase(0,index+1);
	}

	void chaptersInit(char buf[1000], int chapterNum)
	{
		string b(buf); // begin sending a buffer text to class member
		buffer=b;	   // end

		int index=0;
		index=buffer.find(';');
		chaptersHeader.push_back(buffer.substr(0,index)); // Header of current Chapter
		buffer=buffer.erase(0,index+1); //delete Chapter Header

		/*Write the sequance of 3 vectors: */
		while (!buffer.empty())
		{
			chapterNumber.push_back(chapterNum);
			parseElement(1);
			parseElement(2);
		}
	}

	void readFile (string fName)
	{
		int elCount = 1000; // readed string size
 		char* buf = new char[elCount];
		
		/* < begin read file to string */
		
		ifstream t(fName);
		int i=1;
		t.getline(buf,elCount); // ignore first line of csv-structure	
		while(!t.eof())
		{
			t.getline(buf,elCount);
			/* end read file to string > */
			chaptersInit(buf, i);
			i++;
		}
		t.clear();
		t.close();	
	}

	void clearChapters(string fName)
	{
		//currentElementNum=0;
		chaptersHeader.clear();
		buffer="";
		chapterNumber.clear();
		chapterChoose.clear();
		chapterLink.clear();
	}

	chaptersModel (string fName)
	{
		readFile(fName);
	}

	~chaptersModel()
	{
		chaptersHeader.clear();
		chapterNumber.clear();
		chapterChoose.clear();
		chapterLink.clear();
	}

};

//--------------------------------------------------------------
class Model
{
public:
	
	string fileName;
	chaptersModel *chapters;	//file is an array of chapters
	vector <string> output(int chapterNumber, string fName) // form a output vector for View
	{
		vector <string> output;  //result
		if(fileName!=fName) // if current file was change - update entire Model
		{
			clearModel(fName);
			chapters->readFile(fName);
		}

		output.push_back(chapters->chaptersHeader[chapterNumber-1]); // push Header of needed chapter
		
		int index=0;
		while(chapters->chapterNumber[index]!=chapterNumber)	//find first index of chapter choose
		{
			index++;
		}
		int size=chapters->chapterChoose.size();				//need for check the last chapter in file
		while(chapters->chapterNumber[index]==chapterNumber && index<size-1)	//push all chooses
		{
			output.push_back(chapters->chapterChoose[index]);
			index++;	
		}
		return output;
	}

	void clearModel(string fName)	//clear Model before next file load
	{
		fileName=fName;
		chapters->clearChapters(fName);
	}

	string getLink(int currentChapter, int answer) //Link of the next chapteer
	{
		int index=0;
		while(chapters->chapterNumber[index]!=currentChapter)	//find index of begining the links
		{
			index++;
		}
		return chapters->chapterLink[index+answer-1];		//result is a index of begining + inputed answer
	} 
	
	Model(string fName)
	{
		fileName=fName;
		chapters = new chaptersModel(fName); 
	}
	~Model()
	{
		delete chapters;
	}
};

//--------------------------------------------------------------
//--------------------------------------------------------------
//--------------------------------------------------------------

class View
{
	vector <string> printVector;	//Vector for printing
public:
	int print(vector <string> input)
	{
		int size = input.size();	//size for a cycle
		int variantCount=0;
		system("Color 3");	//change console color (Some graphics effects - because it`s View :-) )
		cout<<endl<<input[0]<<endl;	//prit header
		if(size!=1)	//check for chooses
		{
			int i=1;
			while (i<size)
			{
				if(input[i].find_first_not_of(" ")!=-1) // check for only spaces include strings. If not - print
				{
					cout<<'\t'<<i<<". "<<input[i]<<endl;	//print choose
					variantCount++;
				}
				i++;
			}	
		}
		return variantCount;	
	}
	View()
	{

	}
	~View()
	{
		printVector.clear();
	}
};

//--------------------------------------------------------------
//--------------------------------------------------------------
//--------------------------------------------------------------

class Controller
{
	int currentChapter;	//need for update the Model
	string currentFile; //need for update the Model
public:

	bool stop;			//defines, when application must close (Win or Game Over)
	int readAnswer(int answersCount)	//user interaction. answersCount is for checking "against dummy"
	{
		bool correct=false;	//flag for user answer. If false - entering the answer again
		string answer="";
		int result=0;	//the answer`s number

		if(answersCount==0)	// Only for chapters without choose (Win or Game Over)
			stop=true; 

		else
		{
			while(!correct)
			{
				cout<<"Your answer: "<<endl;
				cin>>answer;
				size_t found=answer.find_first_not_of("1234567890"); // check for only numeric strings
				if(found==string::npos)	// if string numeric, then:
				{
					result=stoi(answer); // convert STRING to INT
					if (result<=answersCount && result>0)	
						correct=true;	//change flag
					else cout<<"Wrong answer! ";
				}
				else cout<<"Wrong answer! ";
			}
			return result;
		}
	}
	void update(string link)
	{
		if(link[1]==':')
		{
			if (link[0]=='c')	//if link - chapter:
			{
				currentChapter=stoi(link.erase(0,2));
			}
			if (link[0]=='f')	//if link - filename:
			{
				currentFile=link.erase(0,2)+".csv";
				currentChapter=1;	//new file begins from first chapter
			}
		}
		else
		{
			cout<<"Error: file is damaged. Wrong link.";
			stop=true;
		}
	}
	int getCurrentChapter()	//need for update the Model
	{
		return currentChapter;
	}
	string getCurrentFile() //need for update the Model
	{
		return currentFile;
	}
	Controller(string file)
	{
		currentChapter=1;
		currentFile=file;
		stop=false;
	}
	~Controller() {};
};

//--------------------------------------------------------------
//--------------------------------------------------------------
//--------------------------------------------------------------